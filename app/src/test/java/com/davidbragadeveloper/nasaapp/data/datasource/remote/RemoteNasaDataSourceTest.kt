package com.davidbragadeveloper.nasaapp.data.datasource.remote

import com.davidbragadeveloper.nasaapp.data.datasource.NasaDataSource
import com.davidbragadeveloper.nasaapp.data.datasource.remote.restapi.*
import com.davidbragadeveloper.nasaapp.data.models.CameraDetails
import com.davidbragadeveloper.nasaapp.data.models.Either
import com.davidbragadeveloper.nasaapp.data.models.MarsPhoto
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import retrofit2.Response
import java.util.*

class RemoteNasaDataSourceTest{


    private val nasaApiServiceMock = mock<NasaApiService>()
    private val testObserver = TestObserver<List<MarsPhoto>> ()
    private lateinit var marsPhotos: List<MarsPhoto>
    private lateinit var mockedResponse: MarsPhotosEntitiesWrapper
    private lateinit var retrofitAdapterMock: RetrofitAdapter
    private lateinit var nasaDataSource: NasaDataSource
    private lateinit var marsPhotosSuccess: List<MarsPhoto>
    private lateinit var mockedResponseSuccess: MarsPhotosEntitiesWrapper




    @Before
    fun setUp (){
         marsPhotos  = (1..20)
            .map{
                MarsPhoto(
                    id = it.toLong(),
                    date = Either.Error(Error("unknown date")),
                    imageUrl = "$it",
                    rover = "rover $it",
                    cameraDetails = CameraDetails(
                        id = it.toLong(),
                        name = "name $it",
                        roverId = it.toLong(),
                        fullName = "fullName $it"
                    )
                )
            }


        mockedResponse= MarsPhotosEntitiesWrapper(
            photos = (1..20)
                .map{
                    MarsPhotoEntity(
                        id = it.toLong(),
                        earth_date= "",
                        img_src = "$it",
                        rover = RoverEntity(
                            name = "rover $it"
                        ),
                        camera = CameraDetailsEntity(
                            id = it.toLong(),
                            name = "name $it",
                            rover_id = it.toLong(),
                            full_name = "fullName $it"

                        )
                    )
                }
        )
        val start = Date(0)

        marsPhotosSuccess  = (1..20)
            .map{
                MarsPhoto(
                    id = it.toLong(),
                    date = Either.Success(Date(0)),
                    imageUrl = "$it",
                    rover = "rover $it",
                    cameraDetails = CameraDetails(
                        id = it.toLong(),
                        name = "name $it",
                        roverId = it.toLong(),
                        fullName = "fullName $it"
                    )
                )
            }


        mockedResponseSuccess = MarsPhotosEntitiesWrapper(
            photos = (1..20)
                .map{
                    MarsPhotoEntity(
                        id = it.toLong(),
                        earth_date= "1970-01-01",
                        img_src = "$it",
                        rover = RoverEntity(
                            name = "rover $it"
                        ),
                        camera = CameraDetailsEntity(
                            id = it.toLong(),
                            name = "name $it",
                            rover_id = it.toLong(),
                            full_name = "fullName $it"

                        )
                    )
                }
        )


    }



    @Test
    fun `should getMarsPhotos return the correct data with Either error`() {
        retrofitAdapterMock = mock{
            on{ getApiService() } doReturn (nasaApiServiceMock)
            on { nasaApiServiceMock.getMarsPhotos(
                sol = 2,
                page = 1
            ) } doReturn Single.just(Response.success(mockedResponse))

        }

        nasaDataSource = RemoteNasaDataSource(retrofitAdapterMock)

        nasaDataSource
            .getMarsPhotos()
            .subscribe(testObserver)
        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertValueCount(1)
        val result = testObserver.values()[0]
        assertFalse(result.isEmpty())
        marsPhotos.forEachIndexed{ i, marsPhoto ->
            assertEquals(marsPhoto.id, result[i].id)
            assertTrue(result[i].date is Either.Error)
            assertEquals(marsPhoto.imageUrl, result[i].imageUrl)
        }
    }

    @Test
    fun `should getMarsPhotos return the correct data with Either success`() {
        retrofitAdapterMock = mock{
            on{ getApiService() } doReturn (nasaApiServiceMock)
            on { nasaApiServiceMock.getMarsPhotos(
                sol = 2,
                page = 1
            ) } doReturn Single.just(Response.success(mockedResponse))

        }

        nasaDataSource = RemoteNasaDataSource(retrofitAdapterMock)

        nasaDataSource
            .getMarsPhotos()
            .subscribe(testObserver)
        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertValueCount(1)
        val result = testObserver.values()[0]
        assertFalse(result.isEmpty())
        marsPhotos.forEachIndexed{ i, marsPhoto ->
            assertEquals(marsPhoto.id, result[i].id)
            assertTrue(result[i].date is Either.Error)
            assertEquals(marsPhoto.imageUrl, result[i].imageUrl)
        }
    }


}