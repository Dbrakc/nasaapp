package com.davidbragadeveloper.nasaapp.data.datasource.remote.restapi

import io.reactivex.observers.TestObserver
import org.junit.Assert.assertEquals
import org.junit.Test
import retrofit2.Response

class RetrofitAdapterImpTest {


    private val retrofitAdapter: RetrofitAdapter= RetrofitAdapterImp()
    private val testObserver = TestObserver<Response<MarsPhotosEntitiesWrapper>> ()



    @Test
    fun `should be the api request with the right url with no parameters`() {
        val expectedUrl =
            "https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?api_key=${ApiConstants.Default.API_KEY}" +
                    "&sol=2&page=1"
        retrofitAdapter
            .getApiService()
            .getMarsPhotos()
            .subscribe(testObserver)
        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertValueCount(1)
        val result = testObserver.values()[0]
        assertEquals(expectedUrl, result.raw().request().url().toString())
    }

    @Test
    fun `should be the api request with the right url with parameters`() {
        val expectedUrlWithParameters =
            "https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?api_key=${ApiConstants.Default.API_KEY}" +
                    "&sol=3&page=4"
        retrofitAdapter
            .getApiService()
            .getMarsPhotos(
                sol=3,
                page = 4
            )
            .subscribe(testObserver)
        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertValueCount(1)
        val result = testObserver.values()[0]
        assertEquals(expectedUrlWithParameters, result.raw().request().url().toString())
    }


}