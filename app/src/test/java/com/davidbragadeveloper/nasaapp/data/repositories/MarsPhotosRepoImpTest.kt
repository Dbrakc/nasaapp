package com.davidbragadeveloper.nasaapp.data.repositories

import com.davidbragadeveloper.nasaapp.data.datasource.NasaDataSource
import com.davidbragadeveloper.nasaapp.data.models.CameraDetails
import com.davidbragadeveloper.nasaapp.data.models.Either
import com.davidbragadeveloper.nasaapp.data.models.MarsPhoto
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Test
import java.util.*

class MarsPhotosRepoImpTest {

    private val marsPhotos : List<MarsPhoto> = (1..20)
        .map{
            MarsPhoto(
                id = it.toLong(),
                date = Either.Success(Date()),
                imageUrl = "$it",
                rover = "rover $it",
                cameraDetails = CameraDetails(
                    id = it.toLong(),
                    name = "name $it",
                    roverId = it.toLong(),
                    fullName = "fullName $it"
                )
            )
        }
    private val marsPhotosWithEitherError : List<MarsPhoto> = (1..20)
        .map{
            MarsPhoto(
                id = it.toLong(),
                date = Either.Error(Error()),
                imageUrl = "$it",
                rover = "rover $it",
                cameraDetails = CameraDetails(
                    id = it.toLong(),
                    name = "name $it",
                    roverId = it.toLong(),
                    fullName = "fullName $it"
                )
            )
        }




    private val testObserver = TestObserver<List<MarsPhoto>> ()

    @Test
    fun `should getMarsPhotos return the correct data`() {
        val nasaDataSourceMock = mock<NasaDataSource>{
            on { getMarsPhotos() } doReturn Single.just(marsPhotos)
        }

        val marsPhotosRepo = MarsPhotosRepoImp(nasaDataSource = nasaDataSourceMock)
        marsPhotosRepo
            .getMarsPhotos()
            .subscribe(testObserver)
        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertValueCount(1)
        val result = testObserver.values()[0]
        assertFalse(result.isEmpty())
        marsPhotos.forEachIndexed{ i, marsPhoto ->
            assertEquals(marsPhoto.id, result[i].id)
            assertEquals(marsPhoto.date, result[i].date)
            assertEquals(marsPhoto.imageUrl, result[i].imageUrl)
        }

    }

    @Test
    fun `should getMarsPhotos return the correct data with either error`() {
        val nasaDataSourceMock = mock<NasaDataSource>{
            on { getMarsPhotos() } doReturn Single.just(marsPhotosWithEitherError)
        }

        val marsPhotosRepo = MarsPhotosRepoImp(nasaDataSource = nasaDataSourceMock)
        marsPhotosRepo
            .getMarsPhotos()
            .subscribe(testObserver)
        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertValueCount(1)
        val result = testObserver.values()[0]
        assertFalse(result.isEmpty())
        marsPhotosWithEitherError.forEachIndexed{ i, marsPhoto ->
            assertEquals(marsPhoto.id, result[i].id)
            assertEquals(marsPhoto.date, result[i].date)
            assertEquals(marsPhoto.imageUrl, result[i].imageUrl)
        }


    }

}