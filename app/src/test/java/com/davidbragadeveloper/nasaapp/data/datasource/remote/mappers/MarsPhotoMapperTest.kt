package com.davidbragadeveloper.nasaapp.data.datasource.remote.mappers

import com.davidbragadeveloper.nasaapp.data.datasource.remote.restapi.CameraDetailsEntity
import com.davidbragadeveloper.nasaapp.data.datasource.remote.restapi.MarsPhotoEntity
import com.davidbragadeveloper.nasaapp.data.datasource.remote.restapi.MarsPhotosEntitiesWrapper
import com.davidbragadeveloper.nasaapp.data.datasource.remote.restapi.RoverEntity
import com.davidbragadeveloper.nasaapp.data.models.CameraDetails
import com.davidbragadeveloper.nasaapp.data.models.Either
import com.davidbragadeveloper.nasaapp.data.models.MarsPhoto
import org.junit.Assert.assertEquals
import org.junit.Test
import java.util.*

class MarsPhotoMapperTest{
    private val marsPhotosListWrapper  = MarsPhotosEntitiesWrapper(
        photos = (1..20)
            .map{
                MarsPhotoEntity(
                    id = it.toLong(),
                    img_src = "$it",
                    earth_date = "1970-00-02",
                    rover = RoverEntity(
                        name = "rover $it"
                    ),
                    camera = CameraDetailsEntity(
                        id = it.toLong(),
                        name = "name $it",
                        rover_id = it.toLong(),
                        full_name = "fullName $it"

                    )
                )
            }
    )

    @Test
    fun `should transform  mars photo entity in mars photo`(){

        val expected= (1..20)
            .map{
                MarsPhoto(
                    id = it.toLong(),
                    imageUrl = "$it",
                    date = Either.Success(Date(82800000L)),
                    rover = "rover $it",
                    cameraDetails = CameraDetails(
                        id = it.toLong(),
                        name = "name $it",
                        roverId = it.toLong(),
                        fullName = "fullName $it"
                    )
                )
            }

        val result = MarsPhotoMapper.transformList(marsPhotosListWrapper.photos)

         assertEquals(expected,result)

    }
}