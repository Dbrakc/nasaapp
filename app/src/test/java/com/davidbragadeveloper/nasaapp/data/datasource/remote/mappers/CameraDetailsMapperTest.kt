package com.davidbragadeveloper.nasaapp.data.datasource.remote.mappers

import com.davidbragadeveloper.nasaapp.data.datasource.remote.restapi.CameraDetailsEntity
import com.davidbragadeveloper.nasaapp.data.models.CameraDetails
import org.junit.Assert.assertEquals
import org.junit.Test

class CameraDetailsMapperTest {


    private val cameraDetailsList = (0..20)
        .map{
            CameraDetailsEntity(
                id = it.toLong(),
                name = "name $it",
                rover_id = it.toLong(),
                full_name = "fullName $it"

            )
        }

    @Test
    fun `should transform camera details entity in camera details`(){

        val expected = (0..20)
            .map {
                CameraDetails(
                    id = it.toLong(),
                    name = "name $it",
                    roverId = it.toLong(),
                    fullName = "fullName $it"
                )
            }
        val result = CameraDetailsMapper.transformList(cameraDetailsList)

        assertEquals(expected,result)
    }

}