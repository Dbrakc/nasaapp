package com.davidbragadeveloper.nasaapp.data.models

import org.junit.Assert.*
import org.junit.Test
import java.lang.Error
import java.util.*

class PhotoTest{

    @Test
    fun `should be create Photo with id, date and imageUrl`() {
        assertNotNull(Photo(5, Either.Success(Date()), "kjfa"))
    }

    @Test
    fun `should be create Photo with a either error`() {
        assertNotNull(Photo(5,Either.Error(Error("there's an error")) ,  "kjfa"))
    }

    @Test
    fun `should be create Photo with image default value`() {
        val photo = Photo(5, Either.Success(Date()))
        assertNotNull(photo)
        assertEquals(photo.imageUrl,"")
    }
}