package com.davidbragadeveloper.nasaapp.ui.fragments.marslist

import com.davidbragadeveloper.nasaapp.data.models.CameraDetails
import com.davidbragadeveloper.nasaapp.data.models.Either
import com.davidbragadeveloper.nasaapp.data.models.MarsPhoto
import com.davidbragadeveloper.nasaapp.data.repositories.MarsPhotosRepo
import com.davidbragadeveloper.nasaapp.extdependencies.imageloader.ImageLoader
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Test
import java.util.*



class MarsListViewModelImpImpTest {

    private val marsPhotos : List<MarsPhoto> = (1..20)
        .map{
            MarsPhoto(
                id = it.toLong(),
                date = Either.Success(Date()),
                imageUrl = "$it",
                rover = "rover $it",
                cameraDetails = CameraDetails(
                    id = it.toLong(),
                    name = "name $it",
                    roverId = it.toLong(),
                    fullName = "fullName $it"
                )
            )
        }

    private val marsPhotosWithEitherError : List<MarsPhoto> = (1..20)
        .map{
            MarsPhoto(
                id = it.toLong(),
                date = Either.Success(Date()),
                imageUrl = "$it",
                rover = "rover $it",
                cameraDetails = CameraDetails(
                    id = it.toLong(),
                    name = "name $it",
                    roverId = it.toLong(),
                    fullName = "fullName $it"
                )
            )
        }

    private val testObserver = TestObserver<List<MarsPhoto>> ()


    private val imageLoader = mock<ImageLoader>()




    @Test
    fun `should view model emit a mars photos change event with the correct data`() {
        val marsPhotoRepoMock = mock<MarsPhotosRepo>{
            on { getMarsPhotos() } doReturn Single.just(marsPhotos)
        }

        val viewModel = MarsListViewModelImp(marsPhotosRepo = marsPhotoRepoMock,imageLoader = imageLoader)
        marsPhotoRepoMock.getMarsPhotos().subscribe(testObserver)
        viewModel.loadMarsPhotos()
        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertValueCount(1)
        val result = testObserver.values()[0]
        assertFalse(result.isEmpty())
        marsPhotos.forEachIndexed{ i, marsPhoto ->
            assertEquals(marsPhoto.id, result[i].id)
            assertEquals(marsPhoto.date, result[i].date)
            assertEquals(marsPhoto.imageUrl, result[i].imageUrl)
        }
    }

    @Test
    fun `should view model emit a mars photos change event with the correct data with either error`() {
        val marsPhotoRepoMock = mock<MarsPhotosRepo>{
            on { getMarsPhotos() } doReturn Single.just(marsPhotosWithEitherError)
        }

        val viewModel = MarsListViewModelImp(marsPhotosRepo = marsPhotoRepoMock,imageLoader = imageLoader)
        marsPhotoRepoMock.getMarsPhotos().subscribe(testObserver)
        viewModel.loadMarsPhotos()
        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertValueCount(1)
        val result = testObserver.values()[0]
        assertFalse(result.isEmpty())
        marsPhotosWithEitherError.forEachIndexed{ i, marsPhoto ->
            assertEquals(marsPhoto.id, result[i].id)
            assertEquals(marsPhoto.date, result[i].date)
            assertEquals(marsPhoto.imageUrl, result[i].imageUrl)
        }
    }

}