package com.davidbragadeveloper.nasaapp.navigation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.annotation.IdRes
import androidx.fragment.app.FragmentManager
import com.davidbragadeveloper.nasaapp.ui.activities.marsdetails.MarsDetailsActivity
import com.davidbragadeveloper.nasaapp.ui.fragments.marsdetails.MarsDetailsFragment
import com.davidbragadeveloper.nasaapp.ui.fragments.marslist.MarsListFragment
import com.davidbragadeveloper.nasaapp.utils.Const

object Navigator {
    enum class Restore{
        YES,
        NO
    }

    fun navigateToMarsList(
        fragmentManager: FragmentManager,
        @IdRes container: Int,
        restore: Restore
    ){
        when(restore){
            Restore.NO ->fragmentManager
                .beginTransaction()
                .add(
                    container,
                    MarsListFragment.newInstance(),
                    Const.FragmentTags.MARS_LIST)
                .commit()
            Restore.YES -> fragmentManager
                .beginTransaction()
                .show(fragmentManager.findFragmentByTag(Const.FragmentTags.MARS_LIST)!!)
                .commit()
        }

    }

    fun navigateToMarsDetailFragment(
        fragmentManager: FragmentManager,
        @IdRes container: Int,
        restore: Restore,
        paramsLambda: () -> Bundle
    ){
        val bundle = paramsLambda.invoke()
        when(restore){
            Restore.NO ->fragmentManager
                .beginTransaction()
                .add(
                    container,
                    MarsDetailsFragment.newInstance(bundle),
                    Const.FragmentTags.MARS_DETAILS)
                .commit()
            Restore.YES -> fragmentManager
                .beginTransaction()
                .show(
                    fragmentManager.findFragmentByTag(Const.FragmentTags.MARS_DETAILS)!!
                )
                .commit()
        }
    }

    fun navigateToMarsDetailsActivity(
        context: Context?,
        extrasLambda: () -> Bundle
    ){
        val intent = Intent(context, MarsDetailsActivity::class.java)
        val extras = extrasLambda.invoke()
        intent.putExtras(extras)
        context?.startActivity(intent)

    }

}