package com.davidbragadeveloper.nasaapp.di

import com.davidbragadeveloper.nasaapp.data.datasource.NasaDataSource
import com.davidbragadeveloper.nasaapp.data.datasource.remote.RemoteNasaDataSource
import com.davidbragadeveloper.nasaapp.data.datasource.remote.restapi.RetrofitAdapter
import com.davidbragadeveloper.nasaapp.data.datasource.remote.restapi.RetrofitAdapterImp
import com.davidbragadeveloper.nasaapp.data.repositories.MarsPhotosRepo
import com.davidbragadeveloper.nasaapp.data.repositories.MarsPhotosRepoImp
import com.davidbragadeveloper.nasaapp.extdependencies.imageloader.ImageLoader
import com.davidbragadeveloper.nasaapp.extdependencies.imageloader.picasso.PicassoImageLoader
import com.davidbragadeveloper.nasaapp.ui.fragments.marslist.MarsListViewModelImp
import com.davidbragadeveloper.nasaapp.ui.activities.nasa.NasaViewModelImp
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val appModule = module {

    single<RetrofitAdapter>{ RetrofitAdapterImp() }

    single<NasaDataSource>{ RemoteNasaDataSource (get())}

    single<MarsPhotosRepo>{ MarsPhotosRepoImp(get()) }

    factory <ImageLoader> { PicassoImageLoader() }

    viewModel { MarsListViewModelImp(get(),get()) }

    viewModel { NasaViewModelImp() }

}