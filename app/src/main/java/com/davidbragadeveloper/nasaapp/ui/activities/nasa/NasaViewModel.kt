package com.davidbragadeveloper.nasaapp.ui.activities.nasa

import android.content.Context
import com.davidbragadeveloper.nasaapp.data.models.MarsPhoto

interface NasaViewModel {
    fun marsPhotoClicked(context: Context?, marsPhoto: MarsPhoto)
}