package com.davidbragadeveloper.nasaapp.ui.fragments.marslist

import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.palette.graphics.Palette
import androidx.recyclerview.widget.RecyclerView
import com.davidbragadeveloper.nasaapp.R
import com.davidbragadeveloper.nasaapp.data.models.Either
import com.davidbragadeveloper.nasaapp.data.models.MarsPhoto
import com.davidbragadeveloper.nasaapp.extdependencies.imageloader.ImageLoader
import com.jakewharton.rxbinding3.view.clicks
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.item_mars_photo.view.*
import java.text.DateFormat
import java.util.concurrent.TimeUnit


class MarsListAdapter(
    private val imageLoader: ImageLoader,
    private val onClickElement: ((MarsPhoto) -> Unit)? = null
): RecyclerView.Adapter<MarsListAdapter.ViewHolder>() {

    var list: MutableList<MarsPhoto> = mutableListOf()


    override fun getItemCount(): Int = list.count()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val marsPhoto = list[position]
        holder.bindMarsPhoto(marsPhoto)
    }

    override fun onCreateViewHolder(recyclerView: ViewGroup, viewType: Int): ViewHolder {
        val itemView =
            LayoutInflater.from(recyclerView.context).inflate(R.layout.item_mars_photo, recyclerView, false)
        return ViewHolder(itemView, imageLoader, onClickElement)
    }

    fun setListOfMarsPhotos(newList: List<MarsPhoto>) {
        if(!list.isEmpty()) {
            list.clear()
        }
        list.addAll(newList.toMutableList())
        notifyDataSetChanged()
    }

    fun addMembersToList(marsPhotos: List<MarsPhoto>?) {
        list.addAll(marsPhotos!!.toMutableList())
        notifyDataSetChanged()

    }


    class ViewHolder(
        itemView: View,
        private val imageLoader: ImageLoader,
        private val onClickElement: ((MarsPhoto) -> Unit)?) : RecyclerView.ViewHolder(itemView)
    {

        private val compositeDisposable = CompositeDisposable()

        fun bindMarsPhoto(marsPhoto: MarsPhoto) {
           with(itemView){
               setDate(this, marsPhoto)
               loadImage(marsPhoto)
               subscribeToClickElementEvents(marsPhoto)
           }
        }

        private fun loadImage(marsPhoto: MarsPhoto) {
            imageLoader.loadImage(itemView.imagePoster, marsPhoto.imageUrl) {
                itemView.imagePoster.setImageBitmap(it)
                setColorFrom(it)
            }
        }

        private fun setDate(view: View, marsPhoto: MarsPhoto) {
            with(view) {
                with(marsPhoto.date) {
                    dateLabel.text = when (this) {
                        is Either.Success -> DateFormat.getDateInstance(DateFormat.FULL).format(value)
                        is Either.Error -> context.getString(R.string.unknown_date)
                    }
                }
            }
        }

        private fun subscribeToClickElementEvents(marsPhoto: MarsPhoto) {
            itemView
                .clicks()
                .throttleFirst(600, TimeUnit.MILLISECONDS)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    onClickElement?.invoke(marsPhoto)
                }.addTo(compositeDisposable)
        }

        private fun setColorFrom(bitmap: Bitmap) {
            Palette.from(bitmap).generate {
                val defaultColor = ContextCompat.getColor(itemView.context, R.color.colorPrimary)
                val swatch = it?.vibrantSwatch?: it?.dominantSwatch
                val color = swatch?.rgb ?: defaultColor
                itemView.container.setBackgroundColor(color)
                itemView.dataContainer.setBackgroundColor(color)
            }
        }
    }
}


