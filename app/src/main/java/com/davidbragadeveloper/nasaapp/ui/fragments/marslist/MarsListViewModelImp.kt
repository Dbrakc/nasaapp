package com.davidbragadeveloper.nasaapp.ui.fragments.marslist

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.davidbragadeveloper.nasaapp.data.models.MarsPhoto
import com.davidbragadeveloper.nasaapp.data.repositories.MarsPhotosRepo
import com.davidbragadeveloper.nasaapp.extdependencies.imageloader.ImageLoader
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class MarsListViewModelImp (
    private val marsPhotosRepo: MarsPhotosRepo,
    private val imageLoader: ImageLoader
) : ViewModel(), MarsListViewModel{


    val marsPhotosLoadEvent by lazy {
        MutableLiveData<List<MarsPhoto>>()
    }

    val marsPhotosNewPageEvent by lazy {
        MutableLiveData<List<MarsPhoto>>()
    }

    private val compositeDisposable : CompositeDisposable by lazy {
        CompositeDisposable()
    }

    init {
        loadMarsPhotos()
    }


    override fun loadMarsPhotos(sol: Int, page: Int ) {
        getMarsPhotos(
            sol = sol,
            page = page
        ) {
            marsPhotosLoadEvent.value = it
        }
    }


    override fun updateMarsPhotos(sol: Int, page: Int) {
        getMarsPhotos(
            sol = sol,
            page = page
        ) {
            marsPhotosNewPageEvent.value = it
        }
    }


    override fun getImageLoader(): ImageLoader = imageLoader



    override fun onCleared() {
        compositeDisposable.dispose()
    }

    private fun getMarsPhotos(sol: Int = 2, page: Int=1, onSuccess: (List<MarsPhoto>) -> Unit) =
        marsPhotosRepo
            .getMarsPhotos(
                sol= sol,
                page = page
            )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onSuccess = {
                    onSuccess.invoke(it)
                },
                onError = {
                    Log.e(MarsListViewModelImp::class.simpleName, it.localizedMessage)
                }
            )
            .addTo(compositeDisposable)



}