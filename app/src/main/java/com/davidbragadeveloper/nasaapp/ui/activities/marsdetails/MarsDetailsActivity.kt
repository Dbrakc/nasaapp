package com.davidbragadeveloper.nasaapp.ui.activities.marsdetails

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.davidbragadeveloper.nasaapp.R
import com.davidbragadeveloper.nasaapp.navigation.Navigator


class MarsDetailsActivity : AppCompatActivity() {

    private var params: Bundle?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mars_details)
        params = intent.extras

        if(savedInstanceState==null) {
            showDetailsFragment(Navigator.Restore.NO)
        }else{
            showDetailsFragment(Navigator.Restore.YES)
        }
    }


    private fun showDetailsFragment(restore: Navigator.Restore) {
        Navigator.navigateToMarsDetailFragment(
            fragmentManager= supportFragmentManager,
            container= R.id.containerDetails,
            restore = restore
        ){params ?: Bundle()}
    }

}
