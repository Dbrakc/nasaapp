package com.davidbragadeveloper.nasaapp.ui.fragments.marsdetails


import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.palette.graphics.Palette
import com.davidbragadeveloper.nasaapp.R
import com.davidbragadeveloper.nasaapp.data.models.MarsPhoto
import com.davidbragadeveloper.nasaapp.extdependencies.imageloader.ImageLoader
import com.davidbragadeveloper.nasaapp.utils.Const
import kotlinx.android.synthetic.main.fragment_mars_details.*
import org.koin.android.ext.android.inject


private const val ARG_PARAMS = "params"


class MarsDetailsFragment : Fragment() {

    private val imageLoader: ImageLoader by inject()
    private var params: Bundle? = null
    private var marsPhoto: MarsPhoto? = null


    companion object {
        @JvmStatic
        fun newInstance(params: Bundle) =
            MarsDetailsFragment().apply {
                arguments = Bundle().apply {
                    putBundle(ARG_PARAMS, params)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            params = it.getBundle(ARG_PARAMS)
            marsPhoto = params?.getSerializable(Const.ExtraKeys.MARS_PHOTO) as MarsPhoto
            if(marsPhoto == null){ activity?.finish() }

        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_mars_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpElements()
    }


    private fun setUpElements() {
        roverNameLabel.text = marsPhoto?.rover
        roverIdContentLabel.text = marsPhoto?.cameraDetails?.roverId.toString()
        cameraFullNameContentLabel.text = marsPhoto?.cameraDetails?.fullName
        cameraNameContentLabel.text = marsPhoto?.cameraDetails?.name

        imageLoader.loadImage(
            imageContainer = imgPoster,
            imageUrl = marsPhoto!!.imageUrl
        ){
            imgPoster.setImageBitmap(it)
            setColorFrom(it)
        }
    }

    private fun setColorFrom(bitmap: Bitmap) {
        Palette.from(bitmap).generate {
            val defaultColor = ContextCompat.getColor(context!!, R.color.colorPrimary)
            val swatch = it?.vibrantSwatch?: it?.dominantSwatch
            val color = swatch?.rgb ?: defaultColor
            constraintLayout.setBackgroundColor(color)
            imgPoster.setBackgroundColor(color)
        }
    }
}
