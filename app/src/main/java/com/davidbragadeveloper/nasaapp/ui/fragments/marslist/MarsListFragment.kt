package com.davidbragadeveloper.nasaapp.ui.fragments.marslist


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.davidbragadeveloper.nasaapp.R
import com.davidbragadeveloper.nasaapp.ui.activities.nasa.NasaViewModelImp
import com.davidbragadeveloper.nasaapp.utils.Const
import com.davidbragadeveloper.nasaapp.utils.ItemSeparatorDecorator
import com.davidbragadeveloper.nasaapp.utils.LinearPaginatorRecyclerScrollListener
import kotlinx.android.synthetic.main.fragment_mars_list.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class MarsListFragment : Fragment() {

    private val marsListViewModel:MarsListViewModelImp by viewModel()
    private var actualPage: Int = 1


    private val adapter: MarsListAdapter by lazy {
        val instance = MarsListAdapter(marsListViewModel.getImageLoader()){
            val sharedViewModel = ViewModelProviders.of(this).get(NasaViewModelImp::class.java)
            sharedViewModel.marsPhotoClicked(
                context = context,
                marsPhoto= it
            )
        }
        instance
    }



    companion object {
        @JvmStatic
        fun newInstance() = MarsListFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.let {
            actualPage=savedInstanceState.getInt(Const.SaveInstaceKeys.PAGE,1)
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_mars_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRecyclerView()
    }

    override fun onResume() {
        super.onResume()
        marsListViewModel.marsPhotosLoadEvent.observe(this, Observer {
            adapter.setListOfMarsPhotos(it)
            progress.visibility = View.INVISIBLE
            recyclerList.visibility = View.VISIBLE

        })

        marsListViewModel.marsPhotosNewPageEvent.observe(this, Observer {
            adapter.addMembersToList(it)
        })
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(Const.SaveInstaceKeys.PAGE,actualPage)
    }

    private fun setUpRecyclerView() {
        with(recyclerList) {
            layoutManager = LinearLayoutManager(this@MarsListFragment.context).apply {
                orientation = RecyclerView.VERTICAL
            }
            addItemDecoration(ItemSeparatorDecorator(R.dimen.offset_linear))
            addOnScrollListener(LinearPaginatorRecyclerScrollListener(layoutManager as LinearLayoutManager){
                    actualPage = getPageWithCorrections()
                    if(actualPage<=it) {
                        actualPage = it+1
                        marsListViewModel.updateMarsPhotos(page = actualPage)
                    }else{
                        actualPage++
                        marsListViewModel.updateMarsPhotos(page = actualPage)
                    }
                })
            adapter = this@MarsListFragment.adapter
            setHasFixedSize(true)
        }
    }

    private fun getPageWithCorrections() = ((this@MarsListFragment.adapter.list.size - 1) / 25) + 1


}
