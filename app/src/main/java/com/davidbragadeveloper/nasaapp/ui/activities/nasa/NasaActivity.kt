package com.davidbragadeveloper.nasaapp.ui.activities.nasa

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.davidbragadeveloper.nasaapp.R
import com.davidbragadeveloper.nasaapp.navigation.Navigator
import org.koin.androidx.viewmodel.ext.android.viewModel

class NasaActivity : AppCompatActivity() {

    val nasaViewModel:NasaViewModelImp by viewModel()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nasa)
        if(savedInstanceState==null) {
            showListFragment(Navigator.Restore.NO)
        }else{
            showListFragment(Navigator.Restore.YES)
        }

    }


    private fun showListFragment(restore: Navigator.Restore) {
        Navigator.navigateToMarsList(
            fragmentManager = supportFragmentManager,
            container = R.id.containerList,
            restore = restore
        )
    }


}
