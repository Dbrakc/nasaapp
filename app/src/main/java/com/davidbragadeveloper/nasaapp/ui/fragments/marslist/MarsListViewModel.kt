package com.davidbragadeveloper.nasaapp.ui.fragments.marslist

import com.davidbragadeveloper.nasaapp.extdependencies.imageloader.ImageLoader

interface MarsListViewModel {
    fun loadMarsPhotos(sol: Int = 2, page: Int = 1 )
    fun getImageLoader(): ImageLoader
    fun updateMarsPhotos(sol:Int = 2, page: Int)
}