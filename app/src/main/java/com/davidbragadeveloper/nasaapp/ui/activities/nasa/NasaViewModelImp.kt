package com.davidbragadeveloper.nasaapp.ui.activities.nasa

import android.content.Context
import android.os.Bundle
import androidx.lifecycle.ViewModel
import com.davidbragadeveloper.nasaapp.data.models.MarsPhoto
import com.davidbragadeveloper.nasaapp.navigation.Navigator
import com.davidbragadeveloper.nasaapp.utils.Const

class NasaViewModelImp: ViewModel(), NasaViewModel {

    override fun marsPhotoClicked(context: Context?, marsPhoto: MarsPhoto) {
        if(context!=null){
            navigateToDetailsActivity(context, marsPhoto)
        }
    }

    private fun navigateToDetailsActivity(context: Context,marsPhoto: MarsPhoto) {
        Navigator.navigateToMarsDetailsActivity(context = context) {
            Bundle().apply {
                putSerializable(Const.ExtraKeys.MARS_PHOTO, marsPhoto)
            }
        }
    }
}
