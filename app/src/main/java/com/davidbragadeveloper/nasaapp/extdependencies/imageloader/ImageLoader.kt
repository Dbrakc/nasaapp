package com.davidbragadeveloper.nasaapp.extdependencies.imageloader

import android.graphics.Bitmap
import android.view.View

interface ImageLoader {
    fun loadImage(imageContainer: View, imageUrl: String, targetCallback: ((bitmap: Bitmap) -> Unit)? = null)
}