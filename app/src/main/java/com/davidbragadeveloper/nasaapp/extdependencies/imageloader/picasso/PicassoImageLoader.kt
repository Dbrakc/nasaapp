package com.davidbragadeveloper.nasaapp.extdependencies.imageloader.picasso

import android.graphics.Bitmap
import android.view.View
import com.davidbragadeveloper.nasaapp.extdependencies.imageloader.ImageLoader
import com.davidbragadeveloper.nasaapp.utils.CustomTarget
import com.squareup.picasso.Picasso


class PicassoImageLoader: ImageLoader {

    override fun loadImage(
        imageContainer: View,
        imageUrl: String,
        targetCallback:((bitmap: Bitmap)->Unit)?)
    {

        val target = CustomTarget(successCallback = { bitmap, from ->
            targetCallback?.invoke(bitmap)
        })

        imageContainer.tag = target
        Picasso.get()
            .load(imageUrl)
            .into(target)
    }
}