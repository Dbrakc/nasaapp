package com.davidbragadeveloper.nasaapp

import android.app.Application
import com.davidbragadeveloper.nasaapp.di.appModule
import org.koin.android.ext.android.startKoin

class NasaApp: Application(){

    override fun onCreate() {
        super.onCreate()
        startKoin(this, modules = listOf(appModule))
    }

}