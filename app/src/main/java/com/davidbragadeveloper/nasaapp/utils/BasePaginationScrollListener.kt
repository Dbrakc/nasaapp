package com.davidbragadeveloper.nasaapp.utils

import androidx.recyclerview.widget.RecyclerView

abstract class BasePaginationScrollListener (
    val layoutManager: RecyclerView.LayoutManager,
    val lastVisibleItemPositionCallback: ()->Int,
    val onLoadMoreCallback : (Int) -> Unit
): RecyclerView.OnScrollListener() {

    var visibleItemsBeforeReloading : Int = 5
    private var currentPage = 0
    private var itemsInLastLoad = 0
    private var loading = true
    private val startPageIndex = 0

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        var lastVisibleItemPosition = 0
        val totalItemCount = layoutManager.itemCount

        lastVisibleItemPosition= lastVisibleItemPositionCallback.invoke()

        if (loading && (totalItemCount > itemsInLastLoad)) {
            loading = false
            itemsInLastLoad = totalItemCount
        }

        if (!loading && (lastVisibleItemPosition + visibleItemsBeforeReloading) > totalItemCount
            && recyclerView.adapter!!.itemCount > visibleItemsBeforeReloading) {
            currentPage++
            onLoadMoreCallback.invoke(currentPage)
            loading = true
        }

    }

    fun resetState() {
        this.currentPage = this.startPageIndex
        this.visibleItemsBeforeReloading= 0
        this.loading = true
    }
}
