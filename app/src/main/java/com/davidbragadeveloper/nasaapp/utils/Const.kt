package com.davidbragadeveloper.nasaapp.utils

sealed class Const {

    object ExtraKeys{
        const val MARS_PHOTO = "mars_photo"
    }

    object FragmentTags{
        const val MARS_LIST = "mars_list"
        const val MARS_DETAILS = "mars_details"
    }

    object SaveInstaceKeys{
        const val PAGE = "page"
    }
}