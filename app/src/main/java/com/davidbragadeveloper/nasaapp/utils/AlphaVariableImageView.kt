package com.davidbragadeveloper.nasaapp.utils

import android.content.Context
import android.graphics.Bitmap
import android.util.AttributeSet
import android.widget.ImageView


class AlphaVariableImageView : ImageView {
    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun setImageBitmap(bm: Bitmap?) {

        animate()
            .alpha(0F)
            .setDuration(150)
            .setListener(AnimatorEndListener{
                super@AlphaVariableImageView.setImageBitmap(bm)
                animate()
                    .alpha(1F)
                    .duration = 150
            })

    }
}
