package com.davidbragadeveloper.nasaapp.utils

import androidx.recyclerview.widget.LinearLayoutManager

class LinearPaginatorRecyclerScrollListener (
    linearLayoutManager: LinearLayoutManager,
    onLoadMoreCallback: (Int) -> Unit
): BasePaginationScrollListener(
    layoutManager = linearLayoutManager,
    lastVisibleItemPositionCallback = {
        linearLayoutManager.findLastVisibleItemPosition()
    },
    onLoadMoreCallback = onLoadMoreCallback
) {

    init {
        visibleItemsBeforeReloading
    }

}
