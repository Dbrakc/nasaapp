package com.davidbragadeveloper.nasaapp.utils

import android.graphics.Rect
import android.view.View
import androidx.annotation.DimenRes
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class ItemSeparatorDecorator(@DimenRes private val separationId: Int): RecyclerView.ItemDecoration (){
    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)

        val separation: Int = view.context.resources.getDimensionPixelSize(separationId)
        val position = parent.getChildAdapterPosition(view)

        val items = parent.adapter?.itemCount ?: 0

        if(parent.layoutManager is GridLayoutManager){
            setGridLayout(parent, items, position, separation, outRect)
        }else if (parent.layoutManager is LinearLayoutManager){
            setLinearLayout(position, separation, outRect)
        }
    }


    private fun setLinearLayout(position: Int, separation: Int, outRect: Rect) {
        val top = if (position == 0) separation else 0
        outRect.set(separation, top, separation, separation)
    }

    private fun setGridLayout(
        parent: RecyclerView,
        items: Int,
        position: Int,
        separator: Int,
        outRect: Rect
    ) {
        val columns = (parent.layoutManager as GridLayoutManager).spanCount
        val rows = (items + 1 / columns)
        val column = getColumn(position, columns)
        val row = getRow(position, columns)

        val topOffset = if (row == 1) separator else separator / 2
        val leftOffset = if (column == 1) separator else separator / 2
        val bottomOffset = if (row == rows) separator else separator / 2
        val rightOffset = if (column == columns) separator else separator / 2

        outRect.set(leftOffset, topOffset, rightOffset, bottomOffset)
    }

    private fun getRow(position: Int, columns: Int) =
        Math.ceil((position.toDouble() + 1) / columns.toDouble()).toInt()

    private fun getColumn(position: Int, columns: Int) = (position % columns) + 1

}
