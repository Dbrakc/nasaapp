package com.davidbragadeveloper.nasaapp.data.datasource.remote.restapi

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitAdapterImp:RetrofitAdapter {

    override fun getApiService(): NasaApiService = Retrofit
        .Builder()
        .baseUrl(ApiConstants.Urls.BASE)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(NasaApiService::class.java)

}