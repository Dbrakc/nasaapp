package com.davidbragadeveloper.nasaapp.data.models


import java.io.Serializable
import java.lang.Error
import java.util.*

typealias MarsPhoto = Photo
typealias Rover = String

data class Photo(
    val id: Long,
    val date: Either<Date, Error>,
    val imageUrl: String = "",
    val rover: Rover = "",
    val cameraDetails: CameraDetails = CameraDetails(-1L)
):Serializable

data class CameraDetails(
    val id: Long,
    val name: String = "",
    val roverId: Long = -1L,
    val fullName:String = ""
):Serializable

interface Mapper < in M, out T> {
    fun transform (input: M): T
    fun transformList (input: List<M>): List<T>
}

sealed class Either<out L, out R> :Serializable {
    data class Success<L>(val value: L): Either<L, Nothing>(), Serializable
    data class Error<R>(val value: R): Either<Nothing, R>(), Serializable
}

