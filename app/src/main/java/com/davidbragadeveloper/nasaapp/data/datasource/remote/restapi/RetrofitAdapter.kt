package com.davidbragadeveloper.nasaapp.data.datasource.remote.restapi

interface RetrofitAdapter {
    fun getApiService(): NasaApiService
}