package com.davidbragadeveloper.nasaapp.data.repositories

import com.davidbragadeveloper.nasaapp.data.models.MarsPhoto
import io.reactivex.Single

interface MarsPhotosRepo {

    fun getMarsPhotos(sol:Int = 2, page: Int = 1): Single<List<MarsPhoto>>
}