package com.davidbragadeveloper.nasaapp.data.datasource.remote.restapi

import com.davidbragadeveloper.nasaapp.BuildConfig


sealed class ApiConstants{

    object Urls {
        const val BASE: String = "https://api.nasa.gov/mars-photos/api/"
        private const val VERSION_1: String = "v1"
        const val MARS_PHOTOS: String = "$VERSION_1/rovers/curiosity/photos"
    }

    object Param{
        const val API_KEY : String = "api_key"
        const val SOL: String = "sol"
        const val PAGE: String = "page"
    }

    object Default{
        const val API_KEY: String = BuildConfig.NasaApiKey
        const val SOL: Int= 2
        const val PAGE: Int = 1
    }

}
