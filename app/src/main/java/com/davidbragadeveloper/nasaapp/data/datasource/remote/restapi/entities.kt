package com.davidbragadeveloper.nasaapp.data.datasource.remote.restapi

typealias MarsPhotoEntity = PhotoEntity
typealias MarsPhotosEntitiesWrapper = PhotosEntitiesWrapper

data class PhotosEntitiesWrapper(
    val photos: List<PhotoEntity>
)

data class PhotoEntity(
    val id: Long,
    val earth_date: String,
    val img_src: String,
    val camera: CameraDetailsEntity,
    val rover: RoverEntity
)

data class CameraDetailsEntity(
    val id: Long,
    val name: String,
    val rover_id: Long,
    val full_name: String
)

data class RoverEntity(
    val name: String
)