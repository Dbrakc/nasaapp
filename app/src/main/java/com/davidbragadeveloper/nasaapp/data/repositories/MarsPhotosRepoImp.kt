package com.davidbragadeveloper.nasaapp.data.repositories

import com.davidbragadeveloper.nasaapp.data.datasource.NasaDataSource
import com.davidbragadeveloper.nasaapp.data.models.MarsPhoto
import io.reactivex.Single


class MarsPhotosRepoImp(val nasaDataSource: NasaDataSource) : MarsPhotosRepo {

    override fun getMarsPhotos(sol: Int, page: Int): Single<List<MarsPhoto>> =
        nasaDataSource
            .getMarsPhotos(
                sol = sol,
                page = page
            )

}