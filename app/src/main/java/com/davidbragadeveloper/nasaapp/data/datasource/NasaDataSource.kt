package com.davidbragadeveloper.nasaapp.data.datasource

import com.davidbragadeveloper.nasaapp.data.models.MarsPhoto
import io.reactivex.Single

interface NasaDataSource {
    fun getMarsPhotos(sol: Int=2, page: Int=1): Single<List<MarsPhoto>>
}