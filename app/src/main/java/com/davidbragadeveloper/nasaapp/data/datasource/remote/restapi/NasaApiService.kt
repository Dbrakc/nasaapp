package com.davidbragadeveloper.nasaapp.data.datasource.remote.restapi

import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface NasaApiService {

    @GET(ApiConstants.Urls.MARS_PHOTOS)
    fun getMarsPhotos(
        @Query(ApiConstants.Param.API_KEY) apiKey: String = ApiConstants.Default.API_KEY,
        @Query(ApiConstants.Param.SOL) sol: Int = ApiConstants.Default.SOL,
        @Query(ApiConstants.Param.PAGE) page: Int = ApiConstants.Default.PAGE
    ): Single<Response<MarsPhotosEntitiesWrapper>>

}