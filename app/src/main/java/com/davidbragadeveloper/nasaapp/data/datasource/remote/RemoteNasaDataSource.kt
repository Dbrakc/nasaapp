package com.davidbragadeveloper.nasaapp.data.datasource.remote

import com.davidbragadeveloper.nasaapp.data.datasource.NasaDataSource
import com.davidbragadeveloper.nasaapp.data.datasource.remote.mappers.MarsPhotoMapper
import com.davidbragadeveloper.nasaapp.data.datasource.remote.restapi.RetrofitAdapter
import com.davidbragadeveloper.nasaapp.data.models.MarsPhoto
import io.reactivex.Single


class RemoteNasaDataSource(private val retrofitAdapter: RetrofitAdapter): NasaDataSource {

    override fun getMarsPhotos(sol: Int, page: Int): Single<List<MarsPhoto>> =
        retrofitAdapter
            .getApiService()
            .getMarsPhotos(
                sol = sol,
                page = page
            )
            .map {
                it.body()
            }
            .map {
                it.photos
            }
            .map {
                MarsPhotoMapper.transformList(it)
            }
    }


