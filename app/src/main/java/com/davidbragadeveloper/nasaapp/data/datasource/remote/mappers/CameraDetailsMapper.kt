package com.davidbragadeveloper.nasaapp.data.datasource.remote.mappers

import com.davidbragadeveloper.nasaapp.data.datasource.remote.restapi.CameraDetailsEntity
import com.davidbragadeveloper.nasaapp.data.models.CameraDetails
import com.davidbragadeveloper.nasaapp.data.models.Mapper

object CameraDetailsMapper: Mapper<CameraDetailsEntity,CameraDetails> {

    override fun transform(input: CameraDetailsEntity): CameraDetails =
        CameraDetails(
            id = input.id,
            name = input.name,
            roverId = input.rover_id,
            fullName = input.full_name
        )


    override fun transformList(input: List<CameraDetailsEntity>): List<CameraDetails> =
            input.map {
                transform(it)
            }
}