package com.davidbragadeveloper.nasaapp.data.datasource.remote.mappers


import com.davidbragadeveloper.nasaapp.data.datasource.remote.restapi.MarsPhotoEntity
import com.davidbragadeveloper.nasaapp.data.models.Either
import com.davidbragadeveloper.nasaapp.data.models.Mapper
import com.davidbragadeveloper.nasaapp.data.models.MarsPhoto
import java.util.*

object MarsPhotoMapper: Mapper <MarsPhotoEntity, MarsPhoto> {
    override fun transform(input: MarsPhotoEntity): MarsPhoto {
        return MarsPhoto(
            id = input.id,
            date = when (input.earth_date.isBlank() || input.earth_date.isEmpty()) {
                false -> try {
                        val year = input.earth_date.substring(0, 4).toInt()
                        val month = input.earth_date.substring(5, 7).toInt()
                        val day = input.earth_date.substring(8).toInt()
                        val date = GregorianCalendar(year, month, day).time
                        Either.Success(
                            date
                        )
                    }catch (e: Exception){
                        Either.Error(
                            Error(e.localizedMessage)
                        )
                    }

                true -> Either.Error(Error("unknown date"))
            },
            imageUrl = input.img_src.replace("http","https").trim(),
            rover = input.rover.name,
            cameraDetails = CameraDetailsMapper.transform(input.camera)
        )
    }

    override fun transformList(input: List<MarsPhotoEntity>): List<MarsPhoto> =
        input.map {
            transform(it)
        }
}